package com.leafbank.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.leafbank.model.EmailData;
import com.leafbank.service.email.EmailService;
import java.io.IOException;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {

  @Autowired EmailService emailService;

  private static final Logger LOG = LoggerFactory.getLogger(KafkaConsumer.class);

  /**
   * Listens to the Kafka queue for
   *
   * @param message
   * @param headers
   * @throws IOException
   */
  @KafkaListener(topics = "${kafka.topic.boot}")
  public void receive(@Payload Object message, @Headers MessageHeaders headers) throws IOException {
    LOG.info("received message='{}'", message.toString());

    ConsumerRecord<String, String> payload = (ConsumerRecord<String, String>) message;
    EmailData emailData = new ObjectMapper().readValue(payload.value(), EmailData.class);

    emailService.sendNewAccountConfirmationMessage(emailData);
    headers.keySet().forEach(key -> LOG.info("{}: {}", key, headers.get(key)));
  }
}
