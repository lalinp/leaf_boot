package com.leafbank.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class KafkaProducer {
  private static final Logger LOG = LoggerFactory.getLogger(KafkaProducer.class);

  @Autowired private KafkaTemplate<String, Object> kafkaTemplate;

  @Value("${kafka.topic.boot}")
  private String topic;

  public void send(Object message) {
    LOG.info("sending message='{}' to topic='{}'", message, topic);
    kafkaTemplate.send(topic, message);
  }
}
