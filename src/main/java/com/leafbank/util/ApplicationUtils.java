package com.leafbank.util;

import org.springframework.stereotype.Component;

@Component
public class ApplicationUtils {

  public boolean isValidEmailAddress(String email) {
    String ePattern =
        "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
    java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
    java.util.regex.Matcher m = p.matcher(email);
    return m.matches();
  }

  /**
   * ^ - start with a number [0-9] - use only digits (you can also use \d) {8} - use 8 digits $ -
   * End here. Don't add anything after 8 digits.
   *
   * @param accountNumber
   * @return true if string maches regex - i.e. accountNumber is good
   */
  public boolean isValidAccountNumber(String accountNumber) {
    String regExPattern = "^[0-9]{8}$";
    java.util.regex.Pattern p = java.util.regex.Pattern.compile(regExPattern);
    java.util.regex.Matcher m = p.matcher(accountNumber);
    return m.matches();
  }
}
