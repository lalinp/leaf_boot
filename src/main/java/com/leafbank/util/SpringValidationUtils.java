package com.leafbank.util;

import com.leafbank.exceptions.LeafConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class SpringValidationUtils {

  @Autowired
  @Qualifier("localValidatorFactoryBean")
  Validator validator;

  /**
   * validating input payload
   *
   * @param payload
   * @param validationErrors
   */
  public void validate(Object payload, Errors validationErrors) {
    if (validationErrors != null) {
      ValidationUtils.invokeValidator(validator, payload, validationErrors);
      if (validationErrors.hasErrors()) {
        throw new LeafConstraintViolationException(
            "There are validation errors", validationErrors.getFieldErrors());
      }
    }
  }
}
