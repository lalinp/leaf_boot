package com.leafbank.dao;

import com.leafbank.domain.CreditCardEntity;
import com.leafbank.domain.CustomerEntity;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class CardDAO {

  @PersistenceContext private EntityManager entityManager;

  @Transactional
  public boolean addNewCreditCards(List<CreditCardEntity> creditCardsList) {

    creditCardsList
        .stream()
        .forEach(
            eachCreditCard -> {
              entityManager.persist(eachCreditCard);
              entityManager.flush();
            });

    return true;
  }

  @Transactional
  public CustomerEntity getCustomerById(String customrId) {
    return new CustomerEntity();
  }
}
