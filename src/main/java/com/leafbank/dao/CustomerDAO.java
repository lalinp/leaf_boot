package com.leafbank.dao;

import com.leafbank.domain.CustomerEntity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class CustomerDAO {

  @PersistenceContext private EntityManager entityManager;

  private final String FIND_CUSTOMER_BY_ACCOUNT_NUMBER =
      "select c from CustomerEntity c join AccountEntity a  on(c.customerId = a.customer)"
          + " where a.accountNumber = :accountNnumber";
  private final String FIND_CUSTOMER_BY_EMAIL =
      "select c from CustomerEntity c join ContactDetailsEntity cd  on(c.customerId = cd.customer)"
          + " where cd.email = :email";

  @Transactional(propagation = Propagation.REQUIRED)
  public String addNewCustomer(CustomerEntity customer) {
    entityManager.persist(customer);
    entityManager.flush();
    // entityManager.close();
    return customer.getCustomerId();
  }

  @Transactional(propagation = Propagation.REQUIRED)
  public CustomerEntity getCustomerById(String customerId) {
    return entityManager.find(CustomerEntity.class,customerId);
  }

  @Transactional(propagation = Propagation.REQUIRED)
  public CustomerEntity getCustomerByEmail(String email) {
    return entityManager
        .createQuery(FIND_CUSTOMER_BY_EMAIL, CustomerEntity.class)
        .setParameter("email", email)
        .getSingleResult();
  }

  @Transactional(propagation = Propagation.REQUIRED)
  public CustomerEntity getCustomerByAccountNumber(String accountNumber) {
    return entityManager
        .createQuery(FIND_CUSTOMER_BY_ACCOUNT_NUMBER, CustomerEntity.class)
        .setParameter("accountNnumber", accountNumber)
        .getSingleResult();
  }
}
