package com.leafbank.dao;

import com.leafbank.domain.AccountEntity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

@Repository
public class TransactionDAO {

  @PersistenceContext
  private EntityManager entityManager;

  public AccountEntity getAccountById(String accountNumber){
    return entityManager.find(AccountEntity.class,accountNumber);
  }
}
