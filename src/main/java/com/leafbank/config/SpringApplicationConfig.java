package com.leafbank.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableTransactionManagement
public class SpringApplicationConfig implements WebMvcConfigurer {

  @Bean
  @Qualifier("localValidatorFactoryBean")
  public Validator localValidatorFactoryBean() {
    return new LocalValidatorFactoryBean();
  }
}
