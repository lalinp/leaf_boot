package com.leafbank.exceptions;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Collection;
import org.springframework.stereotype.Component;

@JsonInclude(Include.NON_EMPTY)
@Component
public class ErrorResponse {

  private static final String ERROR_CODE = "error_code";
  private static final String ERROR_REF = "error_ref";
  private static final String ERROR_MESSAGE = "error_message";
  private static final String ERROR_DETAIL = "error_detail";

  @JsonProperty(ERROR_CODE)
  private String errorCode;

  @JsonProperty(ERROR_REF)
  private String errorREf;

  @JsonProperty(ERROR_MESSAGE)
  private String errorMessage;

  @JsonProperty(ERROR_DETAIL)
  private Collection<DetailedMessage> errorDetail;

  public ErrorResponse() {}

  @JsonInclude(Include.NON_EMPTY)
  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  @JsonInclude(Include.NON_EMPTY)
  public String getErrorREf() {
    return errorREf;
  }

  public void setErrorREf(String errorREf) {
    this.errorREf = errorREf;
  }

  @JsonInclude(Include.NON_EMPTY)
  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  @JsonInclude(Include.NON_EMPTY)
  public Collection<DetailedMessage> getErrorDetail() {
    return errorDetail;
  }

  public void setErrorDetail(Collection<DetailedMessage> errorDetail) {
    this.errorDetail = errorDetail;
  }
}
