package com.leafbank.exceptions;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.leafbank.model.JsonFiledName;
import java.util.Objects;

public class DetailedMessage {

  @JsonProperty(JsonFiledName.MESSAGE)
  private String message;

  @JsonProperty(JsonFiledName.FIELD)
  private String field;

  public DetailedMessage() {}

  public DetailedMessage(String message, String field) {
    this.message = message;
    this.field = field;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getField() {
    return field;
  }

  public void setField(String field) {
    this.field = field;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof DetailedMessage)) {
      return false;
    }
    DetailedMessage that = (DetailedMessage) o;
    return message.equals(that.message) && field.equals(that.field);
  }

  @Override
  public int hashCode() {
    return Objects.hash(message, field);
  }
}
