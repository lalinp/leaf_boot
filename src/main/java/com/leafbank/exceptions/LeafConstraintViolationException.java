package com.leafbank.exceptions;

import java.util.List;
import org.springframework.validation.FieldError;

/**
 * Exception class that extends RTE Deals with most errors in the application Invoked by the
 * ControllerSdvice and result sent to the user
 */
public class LeafConstraintViolationException extends RuntimeException {

  private String field;
  private List<FieldError> fieldErrors;

  public LeafConstraintViolationException(String message, List<FieldError> fieldErrors) {
    super(message);
    this.fieldErrors = fieldErrors;
  }

  public LeafConstraintViolationException(String message, String field) {
    super(message);
    this.field = field;
  }

  public LeafConstraintViolationException(String message, Throwable throwable) {
    super(message, throwable);
  }

  public String getField() {
    return field;
  }

  public List<FieldError> getFieldErrors() {
    return fieldErrors;
  }
}
