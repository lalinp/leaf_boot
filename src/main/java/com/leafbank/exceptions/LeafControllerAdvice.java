package com.leafbank.exceptions;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class LeafControllerAdvice extends ResponseEntityExceptionHandler {

  public static final String BAD_REQUEST = "Bad Request";
  public static final String CORRELATION_ID = "CORRELATION-ID";

  /**
   * This exception deals with validation errors in the input payload
   *
   * @param ex
   * @param request
   * @return returns a detiled list of errors and the associated field names all using jackson
   *     shenanigans
   */
  @ExceptionHandler(LeafConstraintViolationException.class)
  protected ResponseEntity<Object> handleLeafConstraintViolationException(
      RuntimeException ex, WebRequest request) {

    LeafConstraintViolationException leafConstraintViolationException =
        (LeafConstraintViolationException) ex;
    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.setErrorMessage(BAD_REQUEST);
    List<DetailedMessage> detailedMessageList =
        leafConstraintViolationException
            .getFieldErrors()
            .stream()
            .map(
                eachFieldError ->
                    new DetailedMessage(
                        eachFieldError.getField(), eachFieldError.getDefaultMessage()))
            .collect(Collectors.toList());
    return handleResponse(ex, request, HttpStatus.BAD_REQUEST, errorResponse);
  }

  /**
   * Builds the error response acts as a place holder for generating the correlation id the field
   * that can be used for the DEVOPS team to look for errors as each request / response will have
   * their own correation Id
   *
   * @param ex
   * @param request
   * @param status
   * @param errorResponse
   * @return ResponseEntity
   */
  public ResponseEntity<Object> handleResponse(
      Exception ex, WebRequest request, HttpStatus status, ErrorResponse errorResponse) {
    HttpHeaders headers = new HttpHeaders();
    headers.add(CORRELATION_ID, UUID.randomUUID().toString());
    return handleExceptionInternal(ex, errorResponse, headers, status, request);
  }

  /**
   * returns the final response back to the user with a series of user friendly error messages
   *
   * @param ex
   * @param body
   * @param headers
   * @param status
   * @param request
   * @return ResponseBody<ErrorResponse>
   */
  public ResponseEntity<Object> handleExceptionInternal(
      Exception ex,
      @Nullable Object body,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {

    return new ResponseEntity(body, headers, status);
  }
}
