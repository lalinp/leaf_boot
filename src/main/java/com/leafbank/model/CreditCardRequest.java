package com.leafbank.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class CreditCardRequest {
  @JsonProperty("credit_cards")
  private List<CreditCard> creditCards;

  public List<CreditCard> getCreditCards() {
    return creditCards;
  }

  public void setCreditCards(List<CreditCard> creditCards) {
    this.creditCards = creditCards;
  }
}
