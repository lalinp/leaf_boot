package com.leafbank.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

// The response object to a successful creation of a new customer
public class CustomerResponse {

  @JsonProperty(JsonFiledName.CUSTOMER_ID)
  @JsonInclude(Include.NON_NULL)
  private String customerId;

  @JsonProperty(JsonFiledName.IBAN)
  @JsonInclude(Include.NON_NULL)
  private String IBAN;

  @JsonProperty(JsonFiledName.SWIFT)
  @JsonInclude(Include.NON_NULL)
  private String swiftCode;

  @JsonProperty(JsonFiledName.ACCOUNT_NUMBER)
  @JsonInclude(Include.NON_NULL)
  private String accountNumber;

  @JsonProperty(JsonFiledName.SORT_CODE)
  @JsonInclude(Include.NON_NULL)
  private String sortCode;

  @JsonProperty(JsonFiledName.NOTES)
  @JsonInclude(Include.NON_NULL)
  private String notes;

  @JsonProperty(JsonFiledName.CUSTOMER_DETAILS)
  @JsonInclude(Include.NON_NULL)
  private CustomerDetails customerDetails;

  @JsonProperty(JsonFiledName.CONTACT_DETAILS)
  @JsonInclude(Include.NON_NULL)
  private ContactsList contactDetails;

  @JsonProperty(JsonFiledName.ACCOUNT_DETAILS)
  @JsonInclude(Include.NON_NULL)
  private AccountsList accountDetails;

  @JsonProperty(JsonFiledName.ADDRESS_DETAILS)
  @JsonInclude(Include.NON_NULL)
  private AddressList addressDetails;

  public CustomerResponse() {}

  public String getCustomerId() {
    return customerId;
  }

  public void setCustomerId(String customerId) {
    this.customerId = customerId;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public String getIBAN() {
    return IBAN;
  }

  public void setIBAN(String IBAN) {
    this.IBAN = IBAN;
  }

  public String getSwiftCode() {
    return swiftCode;
  }

  public void setSwiftCode(String swiftCode) {
    this.swiftCode = swiftCode;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public String getSortCode() {
    return sortCode;
  }

  public void setSortCode(String sortCode) {
    this.sortCode = sortCode;
  }

  public ContactsList getContactDetails() {
    return contactDetails;
  }

  public void setContactDetails(ContactsList contactDetails) {
    this.contactDetails = contactDetails;
  }

  public AccountsList getAccountDetails() {
    return accountDetails;
  }

  public void setAccountDetails(AccountsList accountDetails) {
    this.accountDetails = accountDetails;
  }

  public AddressList getAddressDetails() {
    return addressDetails;
  }

  public void setAddressDetails(AddressList addressDetails) {
    this.addressDetails = addressDetails;
  }

  public CustomerDetails getCustomerDetails() {
    return customerDetails;
  }

  public void setCustomerDetails(CustomerDetails customerDetails) {
    this.customerDetails = customerDetails;
  }

  @Override
  public String toString() {
    return "CustomerResponse{"
        + "customerId='"
        + customerId
        + '\''
        + ", notes='"
        + notes
        + '\''
        + '}';
  }
}
