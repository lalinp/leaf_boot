package com.leafbank.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;

public class CustomerDetails {

  @JsonProperty(JsonFiledName.CUSTOMER_ID)
  @JsonInclude(Include.NON_NULL)
  private String customerId;

  @JsonProperty(JsonFiledName.TITLE)
  @JsonInclude(Include.NON_NULL)
  private String title;

  @JsonProperty(JsonFiledName.FIRST_NAME)
  @JsonInclude(Include.NON_NULL)
  private String firstName;

  @JsonProperty(JsonFiledName.MIDDLE_NAME)
  @JsonInclude(Include.NON_NULL)
  private String middleName;

  @JsonProperty(JsonFiledName.LAST_NAME)
  @JsonInclude(Include.NON_NULL)
  private String lastName;

  @JsonProperty(JsonFiledName.DOB)
  @JsonInclude(Include.NON_NULL)
  private java.util.Date dob;

  @JsonProperty(JsonFiledName.GENDER)
  @JsonInclude(Include.NON_NULL)
  private String gender;

  @JsonProperty(JsonFiledName.MARITAL_STATUS)
  @JsonInclude(Include.NON_NULL)
  private String maritalStatus;

  @JsonProperty(JsonFiledName.NATIONALITY)
  @JsonInclude(Include.NON_NULL)
  private String nationality;

  @JsonProperty(JsonFiledName.BIRTH_COUNTRY)
  @JsonInclude(Include.NON_NULL)
  private String countryOfBirth;

  @JsonProperty(JsonFiledName.RESIDENTIAL_STATUS)
  @JsonInclude(Include.NON_NULL)
  private String residentialStatus;

  public CustomerDetails() {}

  public String getCustomerId() {
    return customerId;
  }

  public void setCustomerId(String customerId) {
    this.customerId = customerId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getMiddleName() {
    return middleName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Date getDob() {
    return dob;
  }

  public void setDob(Date dob) {
    this.dob = dob;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getMaritalStatus() {
    return maritalStatus;
  }

  public void setMaritalStatus(String maritalStatus) {
    this.maritalStatus = maritalStatus;
  }

  public String getNationality() {
    return nationality;
  }

  public void setNationality(String nationality) {
    this.nationality = nationality;
  }

  public String getCountryOfBirth() {
    return countryOfBirth;
  }

  public void setCountryOfBirth(String countryOfBirth) {
    this.countryOfBirth = countryOfBirth;
  }

  public String getResidentialStatus() {
    return residentialStatus;
  }

  public void setResidentialStatus(String residentialStatus) {
    this.residentialStatus = residentialStatus;
  }
}
