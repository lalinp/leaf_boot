package com.leafbank.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.stereotype.Component;

@Component
public class LoginRequest {

  @JsonProperty(JsonFiledName.EMAIL)
  private String email;

  @JsonProperty(JsonFiledName.PASSWORD)
  private String password;

  public LoginRequest() {}

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
