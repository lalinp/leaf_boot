package com.leafbank.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class AddressList {

  @JsonProperty(JsonFiledName.ADDRESS_LIST)
  private List<Address> addressDetails;

  public AddressList() {}

  public List<Address> getAddressDetails() {
    return addressDetails;
  }

  public void setAddressDetails(List<Address> addreessDetails) {
    this.addressDetails = addreessDetails;
  }
}
