package com.leafbank.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ContactDetails {

  @JsonProperty(JsonFiledName.PREFFERED_EMAIL)
  private String email;

  @JsonProperty(JsonFiledName.MOBILE_PHONE)
  private String mobileNumber;

  @JsonProperty(JsonFiledName.HOME_NUMBER)
  private String homeNumber;

  @JsonProperty(JsonFiledName.WORK_NUMBER)
  private String workNumber;

  public ContactDetails() {}

  /*public ContactDetailsEntity(final String email, final String mobileNumber, final String homeNumber, final String
      workNumber){

    this.email = email;
    this.mobileNumber = mobileNumber;
    this.homeNumber = homeNumber;
    this.workNumber = workNumber;
  }*/

  public String getEmail() {
    return email;
  }

  public String getMobileNumber() {
    return mobileNumber;
  }

  public String getHomeNumber() {
    return homeNumber;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public void setMobileNumber(final String mobileNumber) {
    this.mobileNumber = mobileNumber;
  }

  public void setHomeNumber(final String homeNumber) {
    this.homeNumber = homeNumber;
  }

  public String getWorkNumber() {
    return workNumber;
  }

  public void setWorkNumber(final String workNumber) {
    this.workNumber = workNumber;
  }

  @Override
  public String toString() {
    return "ContactDetailsEntity{"
        + "email='"
        + email
        + '\''
        + ", mobileNumber='"
        + mobileNumber
        + '\''
        + ", homeNumber='"
        + homeNumber
        + '\''
        + ", workNumber='"
        + workNumber
        + '\''
        + '}';
  }
}
