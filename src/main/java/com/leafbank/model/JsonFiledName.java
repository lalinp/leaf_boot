package com.leafbank.model;

public class JsonFiledName {

  public static final String ROUTING_NO = "routing_no";
  public static final String CC_ID = "credit_card_no";
  public static final String EXP_MMYY = "expiry_mm_yy";
  public static final String VALID_FROM = "valid_from_mm_yy";
  public static final String CC_ISSUER = "credit_card_issuer";
  public static final String AC_TYPE = "account_type";
  public static final String ACC_OPEN_DATE = "account_opened_date";
  public static final String ACC_STATUS = "account_status";
  public static final String OVERDRAFT = "overdraft";
  public static final String OVERDRAFT_LIMIT = "overdraft_limit";
  public static final String CURRENT_BALANCE = "current_balance";
  public static final String ACCOUNT_DETAILS = "account_details";
  public static final String ACCOUNTS_LIST = "accounts_list";
  public static final String ADDRESS_LIST = "address_list";
  public static final String ADDRESS_DETAILS = "address_details";
  public static final String CONTACT_LIST = "contacts_list";
  public static final String CUSTOMER_DETAILS = "customer_details";
  public static final String TRANSFER_AMOUNT ="transfer_amount" ;
  public static final String ORIGIN_ACCOUNT ="origin_account" ;
  public static final String DESTINATION_SORT_CODE ="destination_sort_code" ;
  public static final String TRANSACTION_DATE ="transaction_date" ;
  public static final String DESTINATION_ACCOUNT ="destination_account" ;
  public static final String ORIGIN_ACT_NOTES ="origin_account_notes" ;
  public static final String DESTINATION_ACT_NOTES ="destination_account_notes";

  /** class that should never be instantiated. not even from within the class */
  private JsonFiledName() {
    throw new AssertionError("Json FieldName class should not be instantiated");
  }

  public static final String FIRST_NAME = "first_name";
  public static final String LAST_NAME = "last_name";
  public static final String TITLE = "title";
  public static final String MIDDLE_NAME = "middle_name";
  public static final String COUNTY = "county";
  public static final String DOB = "dob";
  public static final String GENDER = "gender";
  public static final String MARITAL_STATUS = "marital_status";
  public static final String NATIONALITY = "nationality";
  public static final String BIRTH_COUNTRY = "birth_country";
  public static final String RESIDENTIAL_STATUS = "residential_status";
  public static final String POST_CODE = "post_code";
  public static final String HOUSE_NO_NAME = "house_no_or_name";
  public static final String STREET = "street";
  public static final String TOWN = "town";
  public static final String ADDRESS = "address";
  public static final String EMAIL = "email";
  public static final String CONTACT_DETAILS = "contact_details";
  public static final String MOBILE_PHONE = "mobile_phone";
  public static final String HOME_NUMBER = "home_number";
  public static final String WORK_NUMBER = "work_number";
  public static final String CUSTOMER_ID = "customer_id";
  public static final String NOTES = "notes";
  public static final String MESSAGE = "message";
  public static final String FIELD = "field";
  public static final String IBAN = "IBAN";
  public static final String SWIFT = "SWIFT_CODE";
  public static final String ACCOUNT_NUMBER = "account_number";
  public static final String SORT_CODE = "sort_code";
  public static final String PREFFERED_EMAIL = "preffered_email";
  public static final String EMAIL_BODY = "email_body";
  public static final String TO = "to";
  public static final String SUBJECT_HEADER = "subject_header";
  public static final String PASSWORD = "password";
  public static final String COUNTRY_CODE = "country_code";
  public static final String CURRENCY_CODE = "currency_code";
}
