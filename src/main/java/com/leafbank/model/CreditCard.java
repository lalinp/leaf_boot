package com.leafbank.model;

import com.fasterxml.jackson.annotation.JsonProperty;

// @Component
public class CreditCard {

  public static final String CARD_NUMBER = "CardNumber";
  public static final String ISSUING_NETWORK = "IssuingNetwork";

  @JsonProperty(CARD_NUMBER)
  private String cardNumber;

  @JsonProperty(ISSUING_NETWORK)
  private String issuingNetwork;

  public CreditCard() {}

  public String getCardNumber() {
    return cardNumber;
  }

  public void setCardNumber(String cardNumber) {
    this.cardNumber = cardNumber;
  }

  public String getIssuingNetwork() {
    return issuingNetwork;
  }

  public void setIssuingNetwork(String issuingNetwork) {
    this.issuingNetwork = issuingNetwork;
  }

  @Override
  public String toString() {
    return "CreditCard{"
        + "cardNumber='"
        + cardNumber
        + '\''
        + ", IssuingNetwork='"
        + issuingNetwork
        + '\''
        + '}';
  }
}
