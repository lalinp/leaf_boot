package com.leafbank.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class ContactsList {
  @JsonProperty(JsonFiledName.CONTACT_LIST)
  private List<ContactDetails> contactDetails;

  public ContactsList(List<ContactDetails> contactDetails) {
    this.contactDetails = contactDetails;
  }

  public ContactsList() {}

  public List<ContactDetails> getContactDetails() {
    return contactDetails;
  }

  public void setContactDetails(List<ContactDetails> contactDetails) {
    this.contactDetails = contactDetails;
  }
}
