package com.leafbank.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.leafbank.validation.NameConstraint;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.stereotype.Component;

@Component
public class CustomerRequest {

  @JsonProperty(JsonFiledName.FIRST_NAME)
  @NameConstraint
  private String firstName;

  @JsonProperty(JsonFiledName.TITLE)
  @NotNull
  private String title;

  @JsonProperty(JsonFiledName.MIDDLE_NAME)
  private String middleName;

  @JsonProperty(JsonFiledName.LAST_NAME)
  @NameConstraint
  private String lastName;

  @JsonProperty(JsonFiledName.DOB)
  private String dob;

  @JsonProperty(JsonFiledName.GENDER)
  private String gender;

  @JsonProperty(JsonFiledName.MARITAL_STATUS)
  private String maritalStatus;

  @JsonProperty(JsonFiledName.NATIONALITY)
  private String nationality;

  @JsonProperty(JsonFiledName.BIRTH_COUNTRY)
  private String countryOfBirth;

  @JsonProperty(JsonFiledName.RESIDENTIAL_STATUS)
  private String residentialStatus;

  @JsonProperty(JsonFiledName.CONTACT_DETAILS)
  @Valid
  private ContactDetails contactDetails;

  @JsonProperty(JsonFiledName.ADDRESS)
  @Valid
  private Address address;

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getMiddleName() {
    return middleName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getDob() {
    return dob;
  }

  public void setDob(String dob) {
    this.dob = dob;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getMaritalStatus() {
    return maritalStatus;
  }

  public void setMaritalStatus(String maritalStatus) {
    this.maritalStatus = maritalStatus;
  }

  public String getNationality() {
    return nationality;
  }

  public void setNationality(String nationality) {
    this.nationality = nationality;
  }

  public String getCountryOfBirth() {
    return countryOfBirth;
  }

  public void setCountryOfBirth(String countryOfBirth) {
    this.countryOfBirth = countryOfBirth;
  }

  public String getResidentialStatus() {
    return residentialStatus;
  }

  public void setResidentialStatus(String residentialStatus) {
    this.residentialStatus = residentialStatus;
  }

  public ContactDetails getContactDetails() {
    return contactDetails;
  }

  public void setContactDetails(ContactDetails contactDetails) {
    this.contactDetails = contactDetails;
  }

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }

  @Override
  public String toString() {
    return "CustomerRequest{"
        + "firstName='"
        + firstName
        + '\''
        + ", title='"
        + title
        + '\''
        + ", middleName='"
        + middleName
        + '\''
        + ", lastName='"
        + lastName
        + '\''
        + ", dob='"
        + dob
        + '\''
        + ", gender='"
        + gender
        + '\''
        + ", maritalStatus='"
        + maritalStatus
        + '\''
        + ", nationality='"
        + nationality
        + '\''
        + ", countryOfBirth='"
        + countryOfBirth
        + '\''
        + ", houseNumberOrName='"
        + address.getHouseNumberOrName()
        + '\''
        + ", street='"
        + address.getStreet()
        + '\''
        + ", town='"
        + address.getTown()
        + '\''
        + ", postCode='"
        + address.getPostCode()
        + '\''
        + ", residentialStatus='"
        + residentialStatus
        + '\''
        + ", contactDetails="
        + contactDetails
        + ", address="
        + address
        + '}';
  }
}
