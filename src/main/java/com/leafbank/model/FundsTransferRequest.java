package com.leafbank.model;


import com.fasterxml.jackson.annotation.JsonProperty;

public class FundsTransferRequest {

  @JsonProperty(JsonFiledName.TRANSFER_AMOUNT)
  private String transferAmount;
  @JsonProperty(JsonFiledName.ORIGIN_ACCOUNT)
  private String originAccount;
  @JsonProperty(JsonFiledName.DESTINATION_SORT_CODE)
  private String destinationSortCode;
  @JsonProperty(JsonFiledName.TRANSACTION_DATE)
  private String transactionDate;
  @JsonProperty(JsonFiledName.DESTINATION_ACCOUNT)
  private String destinationAccount;
  @JsonProperty(JsonFiledName.ORIGIN_ACT_NOTES)
  private String originAccountNotes;
  @JsonProperty(JsonFiledName.DESTINATION_ACT_NOTES)
  private String destinationAccountNotes;

  public FundsTransferRequest(){

  }

  public String getTransferAmount() {
    return transferAmount;
  }

  public void setTransferAmount(String transferAmount) {
    this.transferAmount = transferAmount;
  }

  public String getOriginAccount() {
    return originAccount;
  }

  public void setOriginAccount(String originAccount) {
    this.originAccount = originAccount;
  }

  public String getDestinationAccount() {
    return destinationAccount;
  }

  public void setDestinationAccount(String destinationAccount) {
    this.destinationAccount = destinationAccount;
  }

  public String getOriginAccountNotes() {
    return originAccountNotes;
  }

  public void setOriginAccountNotes(String originAccountNotes) {
    this.originAccountNotes = originAccountNotes;
  }

  public String getDestinationAccountNotes() {
    return destinationAccountNotes;
  }

  public void setDestinationAccountNotes(String destinationAccountNotes) {
    this.destinationAccountNotes = destinationAccountNotes;
  }

  public String getDestinationSortCode() {
    return destinationSortCode;
  }

  public void setDestinationSortCode(String destinationSortCode) {
    this.destinationSortCode = destinationSortCode;
  }

  public String getTransactiondate() {
    return transactionDate;
  }

  public void setTransactiondate(String transactionDate) {
    this.transactionDate = transactionDate;
  }

  @Override
  //TODO - use encryption to hide PII data
  public String toString() {
    return "FundsTransferRequest{" +
        "transferAmount='" + transferAmount + '\'' +
        ", originAccount='" + originAccount + '\'' +
        ", destinationAccount='" + destinationAccount + '\'' +
        ", originAccountNotes='" + originAccountNotes + '\'' +
        ", destinationAccountNotes='" + destinationAccountNotes + '\'' +
        '}';
  }
}
