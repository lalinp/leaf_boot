package com.leafbank.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Address {

  @JsonProperty(JsonFiledName.HOUSE_NO_NAME)
  private String houseNumberOrName;

  @JsonProperty(JsonFiledName.STREET)
  private String street;

  @JsonProperty(JsonFiledName.TOWN)
  private String town;

  @JsonProperty(JsonFiledName.POST_CODE)
  private String postCode;

  @JsonProperty(JsonFiledName.COUNTY)
  private String county;

  public Address() {}

  public String getHouseNumberOrName() {
    return houseNumberOrName;
  }

  public void setHouseNumberOrName(String houseNumberOrName) {
    this.houseNumberOrName = houseNumberOrName;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getTown() {
    return town;
  }

  public void setTown(String town) {
    this.town = town;
  }

  public String getPostCode() {
    return postCode;
  }

  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }

  public String getCounty() {
    return county;
  }

  public void setCounty(String county) {
    this.county = county;
  }

  @Override
  public String toString() {
    return "AddressEntity{"
        + "houseNumberOrName='"
        + houseNumberOrName
        + '\''
        + ", street='"
        + street
        + '\''
        + ", town='"
        + town
        + '\''
        + ", postCode='"
        + postCode
        + '\''
        + ", county='"
        + county
        + '\''
        + '}';
  }
}
