package com.leafbank.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Map;

/** The Email Model object for passing Email objects thru Kafka to the mail service */
public class EmailData {

  @JsonProperty(JsonFiledName.TO)
  private String to;

  @JsonProperty(JsonFiledName.SUBJECT_HEADER)
  private String subject;

  @JsonProperty(JsonFiledName.EMAIL_BODY)
  private Map messageBody;

  public EmailData(final String to, final String subjectHeader, final Map emailMessageBody) {
    this.to = to;
    this.subject = subjectHeader;
    this.messageBody = emailMessageBody;
  }

  public EmailData() {}

  public String getTo() {
    return to;
  }

  public String getSubject() {
    return subject;
  }

  public Map getMessageBody() {
    return messageBody;
  }

  public void setTo(String to) {
    this.to = to;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public void setMessageBody(Map messageBody) {
    this.messageBody = messageBody;
  }
}
