package com.leafbank.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.Date;

public class AccountDetails {

  @JsonProperty(JsonFiledName.ACCOUNT_NUMBER)
  private String accountNumber;

  @JsonProperty(JsonFiledName.SORT_CODE)
  private String sortCode;

  @JsonProperty(JsonFiledName.COUNTRY_CODE)
  private String countryCode;

  @JsonProperty(JsonFiledName.CURRENCY_CODE)
  private String currencyCode;

  @JsonProperty(JsonFiledName.ROUTING_NO)
  private String routingNumber;

  @JsonProperty(JsonFiledName.CC_ID)
  private String creditCardId;

  @JsonProperty(JsonFiledName.EXP_MMYY)
  private String expMMYY;

  @JsonProperty(JsonFiledName.VALID_FROM)
  private String validFromMMYY;

  @JsonProperty(JsonFiledName.CC_ISSUER)
  private String cardIssuer;

  @JsonProperty(JsonFiledName.AC_TYPE)
  private String accountType;

  @JsonProperty(JsonFiledName.ACC_OPEN_DATE)
  private Date accountOpenDate;

  @JsonProperty(JsonFiledName.ACC_STATUS)
  private String accountStatus;

  @JsonProperty(JsonFiledName.OVERDRAFT)
  private int overdraftStatus;

  @JsonProperty(JsonFiledName.OVERDRAFT_LIMIT)
  private BigDecimal overDraftLimit;

  @JsonProperty(JsonFiledName.CURRENT_BALANCE)
  private BigDecimal currentBalance;

  @JsonProperty(JsonFiledName.IBAN)
  private String IBANNumber;

  @JsonProperty(JsonFiledName.SWIFT)
  private String SWIFTNumber;

  public AccountDetails() {}

  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public String getSortCode() {
    return sortCode;
  }

  public void setSortCode(String sortCode) {
    this.sortCode = sortCode;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public String getRoutingNumber() {
    return routingNumber;
  }

  public void setRoutingNumber(String routingNumber) {
    this.routingNumber = routingNumber;
  }

  public String getCreditCardId() {
    return creditCardId;
  }

  public void setCreditCardId(String creditCardId) {
    this.creditCardId = creditCardId;
  }

  public String getExpMMYY() {
    return expMMYY;
  }

  public void setExpMMYY(String expMMYY) {
    this.expMMYY = expMMYY;
  }

  public String getValidFromMMYY() {
    return validFromMMYY;
  }

  public void setValidFromMMYY(String validFromMMYY) {
    this.validFromMMYY = validFromMMYY;
  }

  public String getCardIssuer() {
    return cardIssuer;
  }

  public void setCardIssuer(String cardIssuer) {
    this.cardIssuer = cardIssuer;
  }

  public String getAccountType() {
    return accountType;
  }

  public void setAccountType(String accountType) {
    this.accountType = accountType;
  }

  public Date getAccountOpenDate() {
    return accountOpenDate;
  }

  public void setAccountOpenDate(Date accountOpenDate) {
    this.accountOpenDate = accountOpenDate;
  }

  public String getAccountStatus() {
    return accountStatus;
  }

  public void setAccountStatus(String accountStatus) {
    this.accountStatus = accountStatus;
  }

  public int getOverdraftStatus() {
    return overdraftStatus;
  }

  public void setOverdraftStatus(int overdraftStatus) {
    this.overdraftStatus = overdraftStatus;
  }

  public BigDecimal getOverDraftLimit() {
    return overDraftLimit;
  }

  public void setOverDraftLimit(BigDecimal overDraftLimit) {
    this.overDraftLimit = overDraftLimit;
  }

  public BigDecimal getCurrentBalance() {
    return currentBalance;
  }

  public void setCurrentBalance(BigDecimal currentBalance) {
    this.currentBalance = currentBalance;
  }

  public String getIBANNumber() {
    return IBANNumber;
  }

  public void setIBANNumber(String IBANNumber) {
    this.IBANNumber = IBANNumber;
  }

  public String getSWIFTNumber() {
    return SWIFTNumber;
  }

  public void setSWIFTNumber(String SWIFTNumber) {
    this.SWIFTNumber = SWIFTNumber;
  }

  @Override
  public String toString() {
    return "AccountDetails{"
        + "accountNumber='"
        + accountNumber
        + '\''
        + ", sortCode='"
        + sortCode
        + '\''
        + ", countryCode='"
        + countryCode
        + '\''
        + ", currencyCode='"
        + currencyCode
        + '\''
        + ", routingNumber='"
        + routingNumber
        + '\''
        + ", creditCardId='"
        + creditCardId
        + '\''
        + ", expMMYY='"
        + expMMYY
        + '\''
        + ", validFromMMYY='"
        + validFromMMYY
        + '\''
        + ", cardIssuer='"
        + cardIssuer
        + '\''
        + ", accountType='"
        + accountType
        + '\''
        + ", accountOpenDate="
        + accountOpenDate
        + ", accountStatus='"
        + accountStatus
        + '\''
        + ", IBANNumber='"
        + IBANNumber
        + '\''
        + ", SWIFTNumber='"
        + SWIFTNumber
        + '\''
        + '}';
  }
}
