package com.leafbank.model;


public class FundsTransferResponse {

  private String transferAmount;
  private String originAccount;
  private String destinationAccount;
  private String notes;

  public FundsTransferResponse(){

  }

  public String getTransferAmount() {
    return transferAmount;
  }

  public void setTransferAmount(String transferAmount) {
    this.transferAmount = transferAmount;
  }

  public String getOriginAccount() {
    return originAccount;
  }

  public void setOriginAccount(String originAccount) {
    this.originAccount = originAccount;
  }

  public String getDestinationAccount() {
    return destinationAccount;
  }

  public void setDestinationAccount(String destinationAccount) {
    this.destinationAccount = destinationAccount;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  @Override
  //TODO - use encryption to hide PII data
  public String toString() {
    return "FundsTransferResponse{" +
        "transferAmount='" + transferAmount + '\'' +
        ", originAccount='" + originAccount + '\'' +
        ", destinationAccount='" + destinationAccount + '\'' +
        ", notes='" + notes + '\'' +
        '}';
  }
}
