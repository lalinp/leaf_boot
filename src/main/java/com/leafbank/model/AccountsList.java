package com.leafbank.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class AccountsList {

  @JsonProperty(JsonFiledName.ACCOUNTS_LIST)
  private List<AccountDetails> accountDetailsList;

  public AccountsList() {}

  public List<AccountDetails> getAccountDetailsList() {
    return accountDetailsList;
  }

  public void setAccountDetailsList(List<AccountDetails> accountDetailsList) {
    this.accountDetailsList = accountDetailsList;
  }

  @Override
  public String toString() {
    return "AccountsList{" + "accountDetailsList=" + accountDetailsList + '}';
  }
}
