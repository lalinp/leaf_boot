package com.leafbank.service.email;

import com.leafbank.model.EmailData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class EmailService {

  public static final String NEW_CUSTOMER_EMAIL_INTRO =
      "Welcome to Leaf Bank - Your Customer and Account Details Have been saved. Your MasterCard is on its way! Pleae keep the below information "
          + "safe "
          + "for future use. You can access your profile at anytime to view this information";
  private static final String NEW_CUSTOMER_END_NOTES =
      "Your username and password will be arriving soon in another email. So, Thank you for choosing Leaf Bank. Hope you "
          + "have a enjoyable banking "
          + "experience with "
          + "us. ";

  @Autowired public JavaMailSender emailSender;

  /**
   * send an email with account details when a new customer is onboarded
   *
   * @param emailData
   */
  public void sendNewAccountConfirmationMessage(EmailData emailData) {

    SimpleMailMessage message = new SimpleMailMessage();
    message.setTo(emailData.getTo());
    message.setSubject(emailData.getSubject());
    message.setText(buildNewAccountConfirmationMessage(emailData));
    emailSender.send(message);
  }

  /**
   * Returns the String message body for new account creation pathway. THIS IS PHASE 1 PHASE TWO -
   * explore using a standard template to populate specific information
   *
   * @param emailData
   * @return String value of the email message body
   */
  protected String buildNewAccountConfirmationMessage(EmailData emailData) {
    StringBuilder messageText = new StringBuilder();

    messageText.append("Dear ");
    messageText.append(emailData.getMessageBody().get("first_name"));
    messageText.append(System.getProperty("line.separator"));
    messageText.append(System.getProperty("line.separator"));
    messageText.append(NEW_CUSTOMER_EMAIL_INTRO);
    messageText.append(System.getProperty("line.separator"));
    messageText.append(System.getProperty("line.separator"));
    messageText
        .append("Customer_Id:")
        .append(emailData.getMessageBody().get("customer_id"))
        .append(System.getProperty("line.separator"));
    messageText
        .append("Account Number:")
        .append(emailData.getMessageBody().get("account_number"))
        .append(System.getProperty("line.separator"));
    messageText
        .append("Sort Code:")
        .append(emailData.getMessageBody().get("sort_code"))
        .append(System.getProperty("line.separator"));
    messageText
        .append("Account Type:")
        .append(emailData.getMessageBody().get("account_type"))
        .append(System.getProperty("line.separator"));
    messageText
        .append("IBAN NO:")
        .append(emailData.getMessageBody().get("IBAN_NO"))
        .append(System.getProperty("line.separator"));
    messageText
        .append("SWIFT:")
        .append(emailData.getMessageBody().get("SWIFT"))
        .append(System.getProperty("line.separator"));
    messageText.append(System.getProperty("line.separator"));
    messageText
        .append(NEW_CUSTOMER_END_NOTES)
        .append(System.getProperty("line.separator"))
        .append(System.getProperty("line.separator"))
        .append(System.getProperty("line.separator"));
    messageText.append("Thank you");
    messageText.append(System.getProperty("line.separator"));
    messageText.append(System.getProperty("line.separator"));
    messageText.append("Augusta Prodworthy, Leaf Customer Service Team");

    return messageText.toString();
  }
}
