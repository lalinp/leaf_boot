package com.leafbank.service;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.leafbank.dao.CustomerDAO;
import com.leafbank.domain.AccountEntity;
import com.leafbank.domain.AddressEntity;
import com.leafbank.domain.ContactDetailsEntity;
import com.leafbank.domain.CustomerEntity;
import com.leafbank.kafka.KafkaProducer;
import com.leafbank.model.AccountDetails;
import com.leafbank.model.AccountsList;
import com.leafbank.model.Address;
import com.leafbank.model.AddressList;
import com.leafbank.model.ContactDetails;
import com.leafbank.model.ContactsList;
import com.leafbank.model.CustomerDetails;
import com.leafbank.model.CustomerRequest;
import com.leafbank.model.CustomerResponse;
import com.leafbank.model.EmailData;
import com.leafbank.util.ApplicationUtils;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomerService {

  public static final String HEADER = "Congratulations on your new Leaf Bank Account!";
  private static final String CUSTOMER_SEACRH_BY_EMAIL_ERROR =
      "getCustomerByEmail() method returned null: It could be that no customer is "
          + "linked to the email address. Check your e-mail address and try again";
  private static final String ACCOUNT_NUMBER_SEEMS_TO_BE_NULL = "Account number seems to be null";
  private static final String INVALID_ACCOUNT_NUMBER = "Invalid Account Numbe";
  private static final String NO_CUSTOMER_FOUND_FOR_GIVEN_ACCOUNT_NUMBER =
      "No customer found for given account number";
  private static final String EMAIL_ADDRESS_REQUIRED = "EMAIL address required";
  private static final String NULL_ARG_PASSED_INTO_THE_METHOD = "Null arg passed into the method";
  private static final String ADDRESS_SET_CANNOT_BE_NULL = "Address set cannot be null";
  private static final String YOUR_COLLECTION_IS_EMPTY = "Your Collection is Empty";
  private static final String NEW_CUSTOMER_REST_NOTES =
      "Welcome to Leaf Bank - Your Customer Details Have been saved - We have shared this information via email as well. Pleae keep"
          + " this information safe for future use. You can access your profile at anytime to retrieve this info";

  @Autowired CustomerDAO customerDAO;

  @Autowired KafkaProducer kafkaProducer;

  @Autowired ApplicationUtils applicationUtils;

  /**
   * adds a new customer to the database along with their contact details and address details The
   * application is set up in such a way that, a customer may have one or more addresses, and a
   * collection of contact details This is achieved using one-to-many relationships between,
   * customer & address and customer & contact details
   *
   * @param payload
   * @return should return the customer id - working on it
   */
  @Transactional(propagation = Propagation.REQUIRED)
  public CustomerResponse addNewCustomer(CustomerRequest payload) {

    CustomerEntity customer = customerEntityBuilder(payload);
    AddressEntity address = addressEntityBuilder(payload);
    ContactDetailsEntity contact = contactDetailsEntityBuilder(payload);

    Set<AddressEntity> addressEntitySet = new HashSet<>();
    addressEntitySet.add(address);
    customer.setAddress(addressEntitySet);

    Set<ContactDetailsEntity> contactDetailsEntitySet = new HashSet<>();
    contactDetailsEntitySet.add(contact);
    customer.setContactDetails(contactDetailsEntitySet);

    AccountEntity accountEntity = accountEntityBuilder();
    Set<AccountEntity> accountEntitySet = new HashSet<>();
    accountEntitySet.add(accountEntity);
    customer.setAccounts(accountEntitySet);

    // You need this to complete the One-To-Many relationships between the tables
    address.setCustomer(customer);
    contact.setCustomer(customer);
    accountEntity.setCustomer(customer);
    String customerId = customerDAO.addNewCustomer(customer);
    CustomerResponse response = new CustomerResponse();
    if (null != customerId) {
      //This NEED CHANGING - DUPLICATE CODE - I KNOW I KNOW! ILL FIX THIS SOON
      response.setCustomerId(customerId);
      response.setIBAN(accountEntity.getIBANNumber());
      response.setSwiftCode(accountEntity.getSWIFTNumber());
      response.setAccountNumber(accountEntity.getAccountNumber());
      response.setSortCode(accountEntity.getSortCode());
      response.setNotes(
          NEW_CUSTOMER_REST_NOTES);


      kafkaProducer.send(setUpEmailMessageForkafka(customerId));
    }
    return response;
  }

  /**
   * set up the email obj
   * for Spring mail API
   * sends this to the customer
   * @param customerId
   * @return EmailData
   */
  protected EmailData setUpEmailMessageForkafka(String customerId){

    CustomerEntity customerEntity = customerDAO.getCustomerById(customerId);
    Map<String, String> emailBody = new HashMap<>();
    emailBody.put("first_name", customerEntity.getFirstName());
    emailBody.put("customer_id", customerEntity.getCustomerId());
    emailBody.put("IBAN_NO", customerEntity.getAccounts().iterator().next().getIBANNumber());
    emailBody.put("SWIFT", customerEntity.getAccounts().iterator().next().getSWIFTNumber());
    emailBody.put("account_number", customerEntity.getAccounts().iterator().next().getAccountNumber());
    emailBody.put("sort_code", customerEntity.getAccounts().iterator().next().getSortCode());
    emailBody.put("account_type", customerEntity.getAccounts().iterator().next().getAccountType());
    return  new EmailData(customerEntity.getContactDetails().iterator().next().getEmail(), HEADER, emailBody);
  }

  /**
   * Builds an accountEntity
   * for persistence
   * @return a new AccountEntity
   */

  protected AccountEntity accountEntityBuilder() {
    AccountEntity accountEntity = new AccountEntity();
    accountEntity.setAccountNumber(String.valueOf(10000000 + new Random().nextInt(90000000)));
    accountEntity.setAccountStatus("active");
    accountEntity.setSortCode("12-34-56");
    accountEntity.setIBANNumber("GB29 LFBK 6126 1331 9268 19");
    accountEntity.setSWIFTNumber("LEFBGB22");
    accountEntity.setCountryCode("UK");
    accountEntity.setCurrencyCode("GBP");
    accountEntity.setAccountType("CA");
    accountEntity.setOverDraftLimit(new BigDecimal(5000.00));
    accountEntity.setCurrentBalance(new BigDecimal(1000.00));

    return accountEntity;
  }

  /**
   * Build the conatcts object
   *
   * @param payload
   * @return returns the contacts object
   */
  protected ContactDetailsEntity contactDetailsEntityBuilder(CustomerRequest payload) {
    ContactDetailsEntity contact = new ContactDetailsEntity();
    contact.setHomeNumber(payload.getContactDetails().getHomeNumber());
    contact.setMobileNumber(payload.getContactDetails().getMobileNumber());
    contact.setWorkNumber(payload.getContactDetails().getWorkNumber());
    contact.setEmail(payload.getContactDetails().getEmail());

    return contact;
  }

  /**
   * builds the customer
   *
   * @param payload
   * @return returns the newly built customer
   */
  protected CustomerEntity customerEntityBuilder(CustomerRequest payload) {
    CustomerEntity customer = new CustomerEntity();
    customer.setTitle(payload.getTitle());
    customer.setFirstName(payload.getFirstName());
    customer.setMiddleName(payload.getMiddleName());
    customer.setCountryOfBirth(payload.getCountryOfBirth());
    customer.setLastName(payload.getLastName());
    customer.setGender(payload.getGender());
    customer.setMaritalStatus(payload.getMaritalStatus());
    customer.setNationality(payload.getNationality());
    customer.setResidentialStatus(payload.getResidentialStatus());
    String dateStr = payload.getDob();
    String pattern = "yyyy-MM-dd";
    SimpleDateFormat formatter = new SimpleDateFormat(pattern);
    try {
      java.sql.Date date = new java.sql.Date(formatter.parse(dateStr).getTime());
      System.out.println(date);
      customer.setDob(date);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return customer;
  }

  /**
   * builds the address
   *
   * @param payload
   * @return Address
   */
  protected AddressEntity addressEntityBuilder(CustomerRequest payload) {

    AddressEntity address = new AddressEntity();
    address.setCounty(payload.getAddress().getCounty());
    address.setHouseNumberOrName(payload.getAddress().getHouseNumberOrName());
    address.setPostCode(payload.getAddress().getPostCode());
    address.setStreet(payload.getAddress().getStreet());
    address.setTown(payload.getAddress().getTown());

    return address;
  }

  /**
   * get customer entity
   * for given email
   * @param email
   * @return return a customer object and eagerly fetched related objects
   */

  @Transactional(propagation = Propagation.REQUIRED)
  public CustomerEntity getCustomerByEmail(final @Nullable String email) {

    checkNotNull(email, EMAIL_ADDRESS_REQUIRED); // Guava
    checkArgument(
        applicationUtils.isValidEmailAddress(email),
        "Your email address seems incorrect - Can you double check please?"); // Guava
    return checkNotNull(customerDAO.getCustomerByEmail(email), CUSTOMER_SEACRH_BY_EMAIL_ERROR);
  }

  /**
   * get the customer object
   * for a given account number
   * @param accountNumber
   * @return customer object and eagerly fetched related objects
   */
  @Transactional(propagation = Propagation.REQUIRED)
  public CustomerResponse getCustomerByAccountNumber(final @Nullable String accountNumber) {

    checkNotNull(accountNumber, ACCOUNT_NUMBER_SEEMS_TO_BE_NULL);
    checkArgument(applicationUtils.isValidAccountNumber(accountNumber), INVALID_ACCOUNT_NUMBER);
    return customerResponseBuilder(
        checkNotNull(
            customerDAO.getCustomerByAccountNumber(accountNumber),
            NO_CUSTOMER_FOUND_FOR_GIVEN_ACCOUNT_NUMBER));
  }

  /**
   * Build the customer response
   * @param email
   * @return CustomerResponse object
   */

  @Transactional(propagation = Propagation.REQUIRED)
  public CustomerResponse gainAccessToAccount(final @Nullable String email) {

    checkNotNull(email, EMAIL_ADDRESS_REQUIRED); // Guava
    checkArgument(
        applicationUtils.isValidEmailAddress(email),
        "Your email address seems incorrect - Can you double check please?"); // Guava
    return customerResponseBuilder(getCustomerByEmail(email));
  }

  /**
   * Builds the CustomerResponse Object
   * from the CustomerEntity
   * for output to the user
   * maintain seperation of concerns, the entity layer
   * is not directly exposed to the customer
   * @param entity
   * @return CustomerResponse
   */

  protected CustomerResponse customerResponseBuilder(final @Nullable CustomerEntity entity) {

    checkNotNull(entity, NULL_ARG_PASSED_INTO_THE_METHOD); // Guava
    CustomerResponse response = new CustomerResponse();
    response.setCustomerId(entity.getCustomerId());
    response.setAccountDetails(getAccountDetailsList(entity.getAccounts()));
    response.setAddressDetails(getAddresDetailsList(entity.getAddress()));
    response.setContactDetails(getContactDetailsList(entity.getContactDetails()));
    response.setCustomerDetails(getCustomerDetails(entity));
    return response;
  }

  /**
   * Build CustomerDetails to be part of the
   * response to the user
   * Maintain seperation of concerns
   * @param entity
   * @return
   */
  protected CustomerDetails getCustomerDetails(final @Nullable CustomerEntity entity) {

    checkNotNull(entity, NULL_ARG_PASSED_INTO_THE_METHOD); // Guava
    CustomerDetails customerDetails = new CustomerDetails();
    customerDetails.setCustomerId(entity.getCustomerId());
    customerDetails.setFirstName(entity.getFirstName());
    customerDetails.setMiddleName(entity.getMiddleName());
    customerDetails.setLastName(entity.getLastName());
    customerDetails.setDob(entity.getDob());
    customerDetails.setCountryOfBirth(entity.getCountryOfBirth());
    customerDetails.setGender(entity.getGender());
    customerDetails.setMaritalStatus(entity.getMaritalStatus());
    customerDetails.setNationality(entity.getNationality());
    customerDetails.setResidentialStatus(entity.getResidentialStatus());
    customerDetails.setTitle(entity.getTitle());

    return customerDetails;
  }

  /**
   * Builds an AccountsList
   * object for customer response
   * Maintain seperation of concerns
   * @param accountsSet
   * @return AccountsList
   */
  @Transactional(propagation = Propagation.REQUIRED)
  protected AccountsList getAccountDetailsList(final @Nullable Set<AccountEntity> accountsSet) {

    checkNotNull(accountsSet, NULL_ARG_PASSED_INTO_THE_METHOD); // Guava
    checkArgument( accountsSet.size()> 0, YOUR_COLLECTION_IS_EMPTY);
    AccountsList accountsList = new AccountsList();

    accountsList.setAccountDetailsList(
        accountsSet
            .stream()
            .map(
                eachAccountEntityInTheStream ->
                    buildAccountDetailsFromAccountEntity(eachAccountEntityInTheStream))
            .collect(Collectors.toList()));

    return accountsList;
  }

  /**
   * Build Generate the Address object
   * for response purposes
   * keep entity objects separate from the response
   * @param addressSet
   * @return AddressList
   */
  @Transactional(propagation = Propagation.REQUIRED)
  protected AddressList getAddresDetailsList(@Nullable Set<AddressEntity> addressSet) {

    checkNotNull(addressSet, ADDRESS_SET_CANNOT_BE_NULL); // Guava
    checkArgument( addressSet.size()> 0, YOUR_COLLECTION_IS_EMPTY);
    AddressList addressList = new AddressList();

    addressList.setAddressDetails(
        addressSet
            .stream()
            .map(
                eachAddressEntityInTheStream ->
                    buildAddressDetailsFromAddressEntity(eachAddressEntityInTheStream))
            .collect(Collectors.toList()));

    return addressList;
  }

  /**
   * Return the list of contacts in an object
   * to build the response for customers
   * uses Guava sweetness to check null and preconditons
   * @param contactsSet
   * @return ContactsList
   */
  @Transactional(propagation = Propagation.REQUIRED)
  protected ContactsList getContactDetailsList(
      final @Nullable Set<ContactDetailsEntity> contactsSet) {

    checkNotNull(contactsSet, ADDRESS_SET_CANNOT_BE_NULL); // Guava
    checkArgument( contactsSet.size() > 0, YOUR_COLLECTION_IS_EMPTY);

    ContactsList contactsList = new ContactsList();

    contactsList.setContactDetails(
        contactsSet
            .stream()
            .map(
                eachContactEntityInTheStream ->
                    buildContactDetailsFromContactEntity(eachContactEntityInTheStream))
            .collect(Collectors.toList()));

    return contactsList;
  }

  /**
   * Build contact details from the ContactEntity
   * detaches the entity layer from the response
   * @param eachContactEntityInTheStream
   * @return ContactDetails
   */

  @Transactional(propagation = Propagation.REQUIRED)
  protected ContactDetails buildContactDetailsFromContactEntity(
      ContactDetailsEntity eachContactEntityInTheStream) {

    checkNotNull(eachContactEntityInTheStream, NULL_ARG_PASSED_INTO_THE_METHOD); // Guava

    ContactDetails contactDetails = new ContactDetails();
    contactDetails.setEmail(eachContactEntityInTheStream.getEmail());
    contactDetails.setHomeNumber(eachContactEntityInTheStream.getHomeNumber());
    contactDetails.setMobileNumber(eachContactEntityInTheStream.getMobileNumber());
    contactDetails.setWorkNumber(eachContactEntityInTheStream.getWorkNumber());

    return contactDetails;
  }

  /**
   * Builds Address from AddressEntity
   * Maintains seperation of concerns
   * @param entity
   * @return Address
   */
  @Transactional(propagation = Propagation.REQUIRED)
  protected Address buildAddressDetailsFromAddressEntity(final @Nullable AddressEntity entity) {

    checkNotNull(entity, NULL_ARG_PASSED_INTO_THE_METHOD); // Guava
    Address address = new Address();
    address.setHouseNumberOrName(entity.getHouseNumberOrName());
    address.setStreet(entity.getStreet());
    address.setTown(entity.getTown());
    address.setCounty(entity.getCounty());
    address.setPostCode(entity.getPostCode());
    return address;
  }

  /**
   *  Builds AccountDetails from AccountEntity
   *  Maintains separation of concerns
   * @param accountEntity
   * @return AccountDetails
   */

  @Transactional(propagation = Propagation.REQUIRED)
  protected AccountDetails buildAccountDetailsFromAccountEntity(
      final @Nullable AccountEntity accountEntity) {

    checkNotNull(accountEntity, NULL_ARG_PASSED_INTO_THE_METHOD); // Guava

    AccountDetails details = new AccountDetails();
    details.setAccountNumber(accountEntity.getAccountNumber());
    details.setAccountType(accountEntity.getAccountType());
    details.setAccountOpenDate(accountEntity.getAccountOpenDate());
    details.setAccountStatus(accountEntity.getAccountStatus());
    details.setCardIssuer(accountEntity.getCardIssuer());
    details.setCreditCardId(accountEntity.getCreditCardId());
    details.setAccountType(accountEntity.getAccountType());
    details.setCountryCode(accountEntity.getCountryCode());
    details.setCurrencyCode(accountEntity.getCurrencyCode());
    details.setCurrentBalance(accountEntity.getCurrentBalance());
    details.setExpMMYY(accountEntity.getExpMMYY());
    details.setIBANNumber(accountEntity.getIBANNumber());
    details.setValidFromMMYY(accountEntity.getValidFromMMYY());
    details.setSortCode(accountEntity.getSortCode());
    details.setOverDraftLimit(accountEntity.getOverDraftLimit());
    details.setOverdraftStatus(accountEntity.getOverdraftStatus());
    details.setRoutingNumber(accountEntity.getRoutingNumber());

    return details;
  }
}
