package com.leafbank.service;

import com.leafbank.dao.CardDAO;
import com.leafbank.domain.CreditCardEntity;
import com.leafbank.model.CreditCardRequest;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CardService {

  @Autowired CardDAO cardDao;

  public boolean addCreditCards(CreditCardRequest payoad) {
    return cardDao.addNewCreditCards(cardListBuilder(payoad));
  }

  protected List<CreditCardEntity> cardListBuilder(CreditCardRequest payload) {

    return payload
        .getCreditCards()
        .stream()
        .map(
            eachCreditCard -> {
              CreditCardEntity cardEntity = new CreditCardEntity();
              cardEntity.setCardNumber(eachCreditCard.getCardNumber());
              cardEntity.setIssuingNetwork(eachCreditCard.getIssuingNetwork());
              cardEntity.setExpMMYY("1025");
              cardEntity.setValidFromMMYY("0419");
              cardEntity.setSecurityCode(String.valueOf(new Random().nextInt(900) + 100));
              return cardEntity;
            })
        .collect(Collectors.toList());
  }
}
