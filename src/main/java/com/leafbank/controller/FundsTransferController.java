package com.leafbank.controller;

import com.leafbank.model.FundsTransferRequest;
import com.leafbank.model.FundsTransferResponse;
import com.leafbank.service.TransactionsService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FundsTransferController {

  @Autowired
  TransactionsService transactionsService;

  @PostMapping("/fundstransfer")
  public ResponseEntity getCustomerByTheirAccountNumber(@Valid @RequestBody FundsTransferRequest payload, Errors validationErrors) {

    return new ResponseEntity<FundsTransferResponse>(
        transactionsService.transferFunds(payload), HttpStatus.OK);
  }
}
