package com.leafbank.controller;

import com.leafbank.model.CreditCardRequest;
import com.leafbank.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NewCardListController {

  @Autowired CardService cardService;

  @PostMapping(value = "/creditcard", produces = "application/json")
  public ResponseEntity addCreditCards(
      @RequestBody CreditCardRequest payload, Errors validationErrors) {
    return new ResponseEntity(cardService.addCreditCards(payload), HttpStatus.OK);
  }
}
