package com.leafbank.controller;

import com.leafbank.model.CustomerResponse;
import com.leafbank.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FindCustomerController {

  @Autowired CustomerService customerService;

  @GetMapping("/customers/{accountNumber}")
  public ResponseEntity getCustomerByTheirAccountNumber(@PathVariable String accountNumber) {

    return new ResponseEntity<CustomerResponse>(
        customerService.getCustomerByAccountNumber(accountNumber), HttpStatus.OK);
  }
}
