package com.leafbank.controller;

import com.leafbank.model.CustomerResponse;
import com.leafbank.model.LoginRequest;
import com.leafbank.service.CustomerService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LogInController {

  @Autowired CustomerService customerService;

  @PostMapping(value = "/login", produces = "application/json")
  public ResponseEntity gainAccessToAccount(
      @Valid @RequestBody LoginRequest payload, Errors validationErrors) {

    return new ResponseEntity<CustomerResponse>(
        customerService.gainAccessToAccount(payload.getEmail()), HttpStatus.OK);
  }
}
