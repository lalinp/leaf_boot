package com.leafbank.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "address_entity")
public class AddressEntity implements Serializable {

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  private String AddressId;

  private String houseNumberOrName;
  private String street;
  private String town;
  private String postCode;
  private String county;

  @ManyToOne
  @JoinColumn(name = "customerId", referencedColumnName = "customerId")
  private CustomerEntity customer;

  public AddressEntity() {}

  public String getHouseNumberOrName() {
    return houseNumberOrName;
  }

  public void setHouseNumberOrName(String houseNumberOrName) {
    this.houseNumberOrName = houseNumberOrName;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getTown() {
    return town;
  }

  public void setTown(String town) {
    this.town = town;
  }

  public String getPostCode() {
    return postCode;
  }

  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }

  public String getCounty() {
    return county;
  }

  public void setCounty(String county) {
    this.county = county;
  }

  public String getAddressId() {
    return AddressId;
  }

  public void setAddressId(String addressId) {
    AddressId = addressId;
  }

  public CustomerEntity getCustomer() {
    return customer;
  }

  public void setCustomer(CustomerEntity customer) {
    this.customer = customer;
  }

  @Override
  public String toString() {
    return "AddressEntity{"
        + "houseNumberOrName='"
        + houseNumberOrName
        + '\''
        + ", street='"
        + street
        + '\''
        + ", town='"
        + town
        + '\''
        + ", postCode='"
        + postCode
        + '\''
        + ", county='"
        + county
        + '\''
        + '}';
  }
}
