package com.leafbank.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.GenericGenerator;

@Entity
public class FundsTransfer implements Serializable {

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  private String transferId;
  @Id
  private AccountEntity toAccount;

  private AccountEntity fromAccount;
  private BigDecimal transferAmount;

  @Temporal(TemporalType.TIMESTAMP)
  private Date TransferDateTime;
  private String authorisationKey;
  private String transferStatus;

  public FundsTransfer() {}

  public String getTransferId() {
    return transferId;
  }

  public void setTransferId(String transferId) {
    this.transferId = transferId;
  }

  public AccountEntity getFromAccount() {
    return fromAccount;
  }

  public void setFromAccount(AccountEntity fromAccount) {
    this.fromAccount = fromAccount;
  }

  public AccountEntity getToAccount() {
    return toAccount;
  }

  public void setToAccount(AccountEntity toAccount) {
    this.toAccount = toAccount;
  }

  public BigDecimal getTransferAmount() {
    return transferAmount;
  }

  public void setTransferAmount(BigDecimal transferAmount) {
    this.transferAmount = transferAmount;
  }

  public Date getTransferDateTime() {
    return TransferDateTime;
  }

  public void setTransferDateTime(Date TransferDateTime) {
    this.TransferDateTime = TransferDateTime;
  }

  public String getAuthorisationKey() {
    return authorisationKey;
  }

  public void setAuthorisationKey(String authorisationKey) {
    this.authorisationKey = authorisationKey;
  }

  public String getTransactionStatus() {
    return transferStatus;
  }

  public void setTransactionStatus(String transferStatus) {
    this.transferStatus = transferStatus;
  }
}
