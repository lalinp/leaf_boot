package com.leafbank.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "credit_card_entity")
public class CreditCardEntity implements Serializable {

  @Id private String cardNumber;
  private String IssuingNetwork;
  private String expMMYY;
  private String validFromMMYY;
  private String securityCode;

  public CreditCardEntity() {}

  public String getCardNumber() {
    return cardNumber;
  }

  public void setCardNumber(String cardNumber) {
    this.cardNumber = cardNumber;
  }

  public String getIssuingNetwork() {
    return IssuingNetwork;
  }

  public void setIssuingNetwork(String issuingNetwork) {
    IssuingNetwork = issuingNetwork;
  }

  public String getExpMMYY() {
    return expMMYY;
  }

  public void setExpMMYY(String expMMYY) {
    this.expMMYY = expMMYY;
  }

  public String getValidFromMMYY() {
    return validFromMMYY;
  }

  public void setValidFromMMYY(String validFromMMYY) {
    this.validFromMMYY = validFromMMYY;
  }

  public String getSecurityCode() {
    return securityCode;
  }

  public void setSecurityCode(String securityCode) {
    this.securityCode = securityCode;
  }
}
