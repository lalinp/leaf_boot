package com.leafbank.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.GenericGenerator;

@Entity
public class AccountAccessLog implements Serializable {

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  private String accessLogId;

  @Temporal(TemporalType.TIMESTAMP)
  private Date lastAccessTimeStamp;

  private String ipAddress;
  private String countryAccessedFrom;
  private String regionAccessedFrom;
  private String deviceType; // PC or smart phone

  @OneToOne
  @JoinColumn(unique = true)
  private CustomerEntity customer;

  public AccountAccessLog(Date lastAccessTimeStamp, CustomerEntity customer) {
    this.lastAccessTimeStamp = lastAccessTimeStamp;
    this.customer = customer;
    this.customer.setAccessLog(this);
  }

  public AccountAccessLog() {}

  public String getAccessLogId() {
    return accessLogId;
  }

  public void setAccessLogId(String accessLogId) {
    this.accessLogId = accessLogId;
  }

  public Date getLastAccessTimeStamp() {
    return lastAccessTimeStamp;
  }

  public void setLastAccessTimeStamp(Date lastAccessTimeStamp) {
    this.lastAccessTimeStamp = lastAccessTimeStamp;
  }

  public String getIpAddress() {
    return ipAddress;
  }

  public void setIpAddress(String ipAddress) {
    this.ipAddress = ipAddress;
  }

  public String getCountryAccessedFrom() {
    return countryAccessedFrom;
  }

  public void setCountryAccessedFrom(String countryAccessedFrom) {
    this.countryAccessedFrom = countryAccessedFrom;
  }

  public String getRegionAccessedFrom() {
    return regionAccessedFrom;
  }

  public void setRegionAccessedFrom(String regionAccessedFrom) {
    this.regionAccessedFrom = regionAccessedFrom;
  }

  public String getDeviceType() {
    return deviceType;
  }

  public void setDeviceType(String deviceType) {
    this.deviceType = deviceType;
  }

  public CustomerEntity getCustomer() {
    return customer;
  }

  public void setCustomer(CustomerEntity customer) {
    this.customer = customer;
  }
}
