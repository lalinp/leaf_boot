package com.leafbank.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "account_entity")
public class AccountEntity implements Serializable {

  @Id private String accountNumber;
  private String sortCode;
  private String countryCode;
  private String currencyCode;
  private String routingNumber;
  private String creditCardId;
  private String expMMYY;
  private String validFromMMYY;
  private String securityCode;
  private String cardIssuer;
  private String accountType;

  @Temporal(TemporalType.TIMESTAMP)
  private Date accountOpenDate;

  private String accountStatus;
  private int overdraftStatus;
  private BigDecimal overDraftLimit;
  private BigDecimal currentBalance;
  private String IBANNumber;
  private String SWIFTNumber;

  @ManyToOne
  @JoinColumn(name = "customerId", referencedColumnName = "customerId")
  private CustomerEntity customer;

  public AccountEntity() {}

  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public String getSortCode() {
    return sortCode;
  }

  public void setSortCode(String sortCode) {
    this.sortCode = sortCode;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public String getRoutingNumber() {
    return routingNumber;
  }

  public void setRoutingNumber(String routingNumber) {
    this.routingNumber = routingNumber;
  }

  public String getCreditCardId() {
    return creditCardId;
  }

  public void setCreditCardId(String creditCardId) {
    this.creditCardId = creditCardId;
  }

  public String getExpMMYY() {
    return expMMYY;
  }

  public void setExpMMYY(String expMMYY) {
    this.expMMYY = expMMYY;
  }

  public String getValidFromMMYY() {
    return validFromMMYY;
  }

  public void setValidFromMMYY(String validFromMMYY) {
    this.validFromMMYY = validFromMMYY;
  }

  public String getSecurityCode() {
    return securityCode;
  }

  public void setSecurityCode(String securityCode) {
    this.securityCode = securityCode;
  }

  public String getCardIssuer() {
    return cardIssuer;
  }

  public void setCardIssuer(String cardIssuer) {
    this.cardIssuer = cardIssuer;
  }

  public String getAccountType() {
    return accountType;
  }

  public void setAccountType(String accountType) {
    this.accountType = accountType;
  }

  public Date getAccountOpenDate() {
    return accountOpenDate;
  }

  public void setAccountOpenDate(Date accountOpenDate) {
    this.accountOpenDate = accountOpenDate;
  }

  public String getAccountStatus() {
    return accountStatus;
  }

  public void setAccountStatus(String accountStatus) {
    this.accountStatus = accountStatus;
  }

  public int getOverdraftStatus() {
    return overdraftStatus;
  }

  public void setOverdraftStatus(int overdraftStatus) {
    this.overdraftStatus = overdraftStatus;
  }

  public BigDecimal getOverDraftLimit() {
    return overDraftLimit;
  }

  public void setOverDraftLimit(BigDecimal overDraftLimit) {
    this.overDraftLimit = overDraftLimit;
  }

  public BigDecimal getCurrentBalance() {
    return currentBalance;
  }

  public void setCurrentBalance(BigDecimal currentBalance) {
    this.currentBalance = currentBalance;
  }

  public String getIBANNumber() {
    return IBANNumber;
  }

  public void setIBANNumber(String IBANNumber) {
    this.IBANNumber = IBANNumber;
  }

  public String getSWIFTNumber() {
    return SWIFTNumber;
  }

  public void setSWIFTNumber(String SWIFTNumber) {
    this.SWIFTNumber = SWIFTNumber;
  }

  public CustomerEntity getCustomer() {
    return customer;
  }

  public void setCustomer(CustomerEntity customer) {
    this.customer = customer;
  }
}
