package com.leafbank.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "contact_details_entity")
public class ContactDetailsEntity implements Serializable {

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  private String contactId;

  @Column(name = "email", unique = true, length = 20, nullable = false)
  private String email;

  private String mobileNumber;
  private String homeNumber;
  private String workNumber;

  @ManyToOne
  @JoinColumn(name = "customerId", referencedColumnName = "customerId")
  private CustomerEntity customer;

  public ContactDetailsEntity() {}

  public String getEmail() {
    return email;
  }

  public String getMobileNumber() {
    return mobileNumber;
  }

  public String getHomeNumber() {
    return homeNumber;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public void setMobileNumber(final String mobileNumber) {
    this.mobileNumber = mobileNumber;
  }

  public void setHomeNumber(final String homeNumber) {
    this.homeNumber = homeNumber;
  }

  public String getWorkNumber() {
    return workNumber;
  }

  public void setWorkNumber(final String workNumber) {
    this.workNumber = workNumber;
  }

  public String getContactId() {
    return contactId;
  }

  public void setContactId(String contactId) {
    this.contactId = contactId;
  }

  public CustomerEntity getCustomer() {
    return customer;
  }

  public void setCustomer(CustomerEntity customer) {
    this.customer = customer;
  }

  @Override
  public String toString() {
    return "ContactDetailsEntity{"
        + "email='"
        + email
        + '\''
        + ", mobileNumber='"
        + mobileNumber
        + '\''
        + ", homeNumber='"
        + homeNumber
        + '\''
        + ", workNumber='"
        + workNumber
        + '\''
        + '}';
  }
}
