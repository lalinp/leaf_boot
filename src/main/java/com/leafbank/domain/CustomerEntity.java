package com.leafbank.domain;

import java.io.Serializable;
import java.sql.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

/**
 * FetchMode isn't only applicable with FetchType.EAGER. The rules are as follows: a) if you don't
 * specify FetchMode, the default is JOIN and FetchType works normal, b) if you specify
 * FetchMode.JOIN explicitly, FetchType is ignored and a query is always eager, c) if you specify
 * FetchMode.SELECT or FetchMode.SUBSELECT, FetchType.Type works normal.
 *
 * <p>A great article: https://www.solidsyntax.be/2013/10/17/fetching-collections-hibernate/
 */
@Entity
@Table(name = "customer_entity")
public class CustomerEntity implements Serializable {
  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  private String customerId;

  private String title;
  private String firstName;
  private String middleName;
  private String lastName;

  @Temporal(TemporalType.TIMESTAMP)
  private java.util.Date dob;

  private String gender;
  private String maritalStatus;
  private String nationality;
  private String countryOfBirth;
  private String residentialStatus;

  @OneToMany(
    targetEntity = ContactDetailsEntity.class,
    cascade = CascadeType.ALL,
    mappedBy = "customer"
  )
  @Fetch(value = FetchMode.SELECT)
  private Set<ContactDetailsEntity> contactDetails;

  @OneToMany(targetEntity = AccountEntity.class, cascade = CascadeType.ALL, mappedBy = "customer")
  @Fetch(value = FetchMode.SELECT)
  private Set<AccountEntity> accounts;

  @OneToMany(targetEntity = AddressEntity.class, cascade = CascadeType.ALL, mappedBy = "customer")
  @Fetch(value = FetchMode.SELECT)
  private Set<AddressEntity> address;

  @OneToOne(targetEntity = AccountAccessLog.class, cascade = CascadeType.ALL, mappedBy = "customer")
  @Fetch(value = FetchMode.SELECT)
  private AccountAccessLog accessLog;

  public CustomerEntity() {}

  public void setCustomerId(String customerId) {
    this.customerId = customerId;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setDob(Date dob) {
    this.dob = dob;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public void setMaritalStatus(String maritalStatus) {
    this.maritalStatus = maritalStatus;
  }

  public void setNationality(String nationality) {
    this.nationality = nationality;
  }

  public void setCountryOfBirth(String countryOfBirth) {
    this.countryOfBirth = countryOfBirth;
  }

  public void setResidentialStatus(String residentialStatus) {
    this.residentialStatus = residentialStatus;
  }

  public String getTitle() {
    return title;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getMiddleName() {
    return middleName;
  }

  public String getLastName() {
    return lastName;
  }

  public java.util.Date getDob() {
    return dob;
  }

  public String getGender() {
    return gender;
  }

  public String getMaritalStatus() {
    return maritalStatus;
  }

  public String getNationality() {
    return nationality;
  }

  public String getCountryOfBirth() {
    return countryOfBirth;
  }

  public Set<AddressEntity> getAddress() {
    return address;
  }

  public String getResidentialStatus() {
    return residentialStatus;
  }

  public Set<ContactDetailsEntity> getContactDetails() {
    return contactDetails;
  }

  public String getCustomerId() {
    return customerId;
  }

  public void setContactDetails(Set<ContactDetailsEntity> contactDetails) {
    this.contactDetails = contactDetails;
  }

  public void setAddress(Set<AddressEntity> address) {
    this.address = address;
  }

  public void setDob(java.util.Date dob) {
    this.dob = dob;
  }

  public Set<AccountEntity> getAccounts() {
    return accounts;
  }

  public void setAccounts(Set<AccountEntity> accounts) {
    this.accounts = accounts;
  }

  public AccountAccessLog getAccessLog() {
    return accessLog;
  }

  public void setAccessLog(AccountAccessLog accessLog) {
    this.accessLog = accessLog;
  }
}
