package com.leafbank.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


public class TransactionEntity implements Serializable {

  /*@Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")*/
  private String transactionId;

  private String merchantId;//links to merchant entity
  private String customerId; // customer id
  private String transactionType; //Bank Trf, Purchase, chq, cash
  private BigDecimal transactionAmount;
  private String transactionCategoryId; // links to category
  private Date transactionTimeStamp;
  private String accountNumber; //links to account entity
}
