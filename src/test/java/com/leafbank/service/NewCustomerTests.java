package com.leafbank.service;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.leafbank.dao.CustomerDAO;
import com.leafbank.domain.AddressEntity;
import com.leafbank.domain.ContactDetailsEntity;
import com.leafbank.domain.CustomerEntity;
import com.leafbank.kafka.KafkaProducer;
import com.leafbank.model.Address;
import com.leafbank.model.ContactDetails;
import com.leafbank.model.CustomerRequest;
import com.leafbank.model.CustomerResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class NewCustomerTests {

  @Spy @InjectMocks CustomerService customerService;

  @Mock CustomerDAO customerDAO;

  @Mock KafkaProducer kafkaProducer;

  CustomerEntity mockCustomerEntity;
  AddressEntity mockAddressEntity;
  ContactDetailsEntity contactDetailsEntity;
  Address mockAddress;
  ContactDetails mockContactDetails;
  CustomerRequest payload;

  @Before
  public void init() {

    mockAddressEntity = mock(AddressEntity.class);
    contactDetailsEntity = mock(ContactDetailsEntity.class);
    mockCustomerEntity = mock(CustomerEntity.class);
    mockAddress = mock(Address.class);
    mockContactDetails = mock(ContactDetails.class);
    payload = new CustomerRequest();
    payload.setDob("1972-08-30");
    payload.setAddress(mockAddress);
    payload.setContactDetails(mockContactDetails);
    payload.setFirstName("Gaylord");
    payload.setLastName("Fokker");
    payload.setGender("M");
  }

  @Test
  public void testAddNewCustomer() throws Exception {
    // Mocks
    CustomerEntity mockCustomer = mock(CustomerEntity.class);
    AddressEntity mockAddressEntity = mock(AddressEntity.class);
    ContactDetailsEntity mockContactDetailsEntity = mock(ContactDetailsEntity.class);
    CustomerRequest mockRequest = mock(CustomerRequest.class);

    // When-Thens
    doReturn(mockCustomer).when(customerService).customerEntityBuilder(mockRequest);
    doReturn(mockAddressEntity).when(customerService).addressEntityBuilder(mockRequest);
    doReturn(mockContactDetailsEntity)
        .when(customerService)
        .contactDetailsEntityBuilder(mockRequest);
    when(customerDAO.addNewCustomer(mockCustomer)).thenReturn("123-456");
    // SUT
    CustomerResponse response = customerService.addNewCustomer(mockRequest);

    // Argument Capture
    ArgumentCaptor<CustomerEntity> customerEntityArgumentCaptor =
        ArgumentCaptor.forClass(CustomerEntity.class);

    // Verifying results
    Mockito.verify(mockAddressEntity, times(1)).setCustomer(customerEntityArgumentCaptor.capture());
    Assert.assertTrue(customerEntityArgumentCaptor.getValue() instanceof CustomerEntity);
    Mockito.verify(mockContactDetailsEntity, times(1))
        .setCustomer(customerEntityArgumentCaptor.capture());
    Assert.assertTrue(customerEntityArgumentCaptor.getValue() instanceof CustomerEntity);
    Assert.assertEquals(
        "Welcome to Leaf Bank - Your Customer Details Have been saved - We have shared this information via email as well. Pleae keep this "
            + "information safe for future use. You can access your profile at anytime to retrieve this info",
        response.getNotes());
    Assert.assertEquals("123-456", response.getCustomerId());
  }

  @Test
  public void testCustomerEntityBuilder() throws Exception {

    CustomerEntity result = customerService.customerEntityBuilder(payload);
    Assert.assertEquals("Gaylord", result.getFirstName());
    Assert.assertTrue(result.getDob() instanceof java.sql.Date);
  }
}
